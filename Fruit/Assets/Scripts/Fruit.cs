﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruit : MonoBehaviour {


    private SpriteRenderer sr;
    public Sprite currentFruit;
    public Sprite[] fruitKind;
    public int currentKind;


    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
    }
	// Use this for initialization
	void Start () {
        currentKind = Random.Range(0, fruitKind.Length);
        sr.sprite = fruitKind[currentKind];
        currentFruit = sr.sprite;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D hitInfo)
    {
        if(hitInfo.gameObject.GetComponent<ClearArea>()!=null)

        {
            Destroy(gameObject);
        }
    }
}
