﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : MonoBehaviour {
    public int health;
    public int damage;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void BeHurt()
    {
        health -= damage;
    }


    void Die()
    {
        if(health <= 0)
        {
            //Game Over
        }
    }
}
