﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitInput : MonoBehaviour {

    public float spawnRate = 1f;
    public GameObject fruitPrefab;
    public float maxSpawnOffsetX;
    // Use this for initialization
    void Start()
    {
        InvokeRepeating("SpawnFruit", 0, spawnRate);
    }

    // Update is called once per frame
    void Update()
    {


    }

    void SpawnFruit()
    {
        Vector3 spawnPosition = transform.position;
        spawnPosition.x += Random.Range(-maxSpawnOffsetX, maxSpawnOffsetX);

        Instantiate(fruitPrefab, spawnPosition, new Quaternion());
    }
}
