﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float rotateRate;
    public float shootSpeed;

    private float rotateDirection=1;
    private float timeCount;
    private float timeChange;
    private SpriteRenderer sr;
    private Rigidbody2D rb;
    private Vector3 originPositon;
    private Quaternion originRotation;
    private bool ifShoot = false;
    // Use this for initialization


	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        timeChange = 130 / rotateRate * 0.02f;
        originPositon = transform.position;
        originRotation = transform.rotation;
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        timeCount += Time.deltaTime;
        if (timeCount >= timeChange)
        {
            timeCount = 0;
            rotateDirection = -rotateDirection;
        }
        transform.Rotate(new Vector3(0, 0, rotateDirection * rotateRate));

      

        if (transform.rotation.z >= 0)
        {
            sr.flipX = true;
        }
        if (transform.rotation.z <= 0)
        {
            sr.flipX = false;
        }

        if (Input.GetMouseButtonDown(0)&&!ifShoot)
        {
            Shoot();           
        }
    }

    void Shoot()
    {
        ifShoot = true;
        GetComponent<Collider2D>().enabled = true;
        rotateDirection = 0;
        rb.velocity = transform.up * shootSpeed;
        Invoke("Reborn",1);
    }

    void Reborn()
    {
        this.transform.position = originPositon;
        this.transform.rotation = originRotation;
        timeCount = 0;
        rb.velocity = new Vector3(0,0,0);
        rotateDirection = 1;
        GetComponent<Collider2D>().enabled = false;
        ifShoot = false;
    }

     void OnTriggerEnter2D(Collider2D hitInfo)
    {
        if (hitInfo.gameObject.GetComponent<Fruit>() != null)
        {
            print(hitInfo.gameObject.GetComponent<Fruit>().currentKind);
            Destroy(hitInfo.gameObject);
        }
    }
}
